
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VMN.
#
# You should have agreed to this license and appendix when
# downloading VMN and received a copy of the license and appendix
# along with VMN. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VMN in any way and you must delete
# VMN immediately.

AC_DEFUN([ACE_CHECK_JAR],[

AC_ARG_ENABLE([$5],
     [  --enable-$5    Use $3.],
     [case "${enableval}" in
       yes) $5=true ;

            # Checks for libraries.
            ACE_CHECK_LOADJAR([$2],[$3],[$4],[$5],[$6],[$7])

	    # Set the path to the jar.
	    AC_SUBST($2,[$3]);;

       no)  $5=false ;;
      esac],[$5=false])
AM_CONDITIONAL([$1], [test x$$5 = xtrue])

])
