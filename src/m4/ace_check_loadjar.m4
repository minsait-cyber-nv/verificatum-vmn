
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VMN.
#
# You should have agreed to this license and appendix when
# downloading VMN and received a copy of the license and appendix
# along with VMN. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VMN in any way and you must delete
# VMN immediately.

AC_DEFUN([ACE_CHECK_LOADJAR],[
AC_REQUIRE([ACE_PROG_JAVA])

AC_ARG_ENABLE([check-$4],
     [  --disable-check-$4    Skip checking that $3 is installed.],
     [],[
ace_res=$($JAVA $JAVAFLAGS -classpath $2:tools/installation TestLoadJar $5 $6)

echo -n "checking for $3... "
if test "x$ace_res" = x;
then
   echo "yes"
else
   echo "no"
   AC_MSG_ERROR([$ace_res

Please make sure that $3 is installed
(visit www.verificatum.com) and its absolute path is provided
to configure as the value of the environment variable $1.

This is only needed to configure for compilation. This environment
variable is not needed after installation. In a typical installation
your configuration command should be:

$1=/usr/local/share/java/$3 ./configure --enable-$4
])
fi
])
])
