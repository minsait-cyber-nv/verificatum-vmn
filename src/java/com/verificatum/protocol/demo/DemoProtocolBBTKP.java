
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.demo;

import com.verificatum.arithm.PGroup;
import com.verificatum.crypto.CryptoKeyGen;
import com.verificatum.eio.Marshalizer;
import com.verificatum.protocol.ProtocolBBT;
import com.verificatum.ui.UI;
import com.verificatum.ui.info.PrivateInfo;
import com.verificatum.ui.info.ProtocolInfo;

/**
 *
 * @author Douglas Wikstrom
 */
@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public abstract class DemoProtocolBBTKP extends ProtocolBBT {

    /**
     * Key generation algorithm.
     */
    protected CryptoKeyGen keygen;

    /**
     * Underlying group.
     */
    protected PGroup pGroup;

    /**
     * Creates a runnable wrapper for the protocol.
     *
     * @param privateInfo Information about this party.
     * @param protocolInfo Information about the protocol executed,
     * including information about other parties.
     * @param ui User interface.
     * @throws Exception If the info instances are malformed.
     */
    public DemoProtocolBBTKP(final PrivateInfo privateInfo,
                             final ProtocolInfo protocolInfo,
                             final UI ui)
        throws Exception {
        super(privateInfo, protocolInfo, ui);

        final String keyGenString = privateInfo.getStringValue("keygen");
        keygen = Marshalizer.unmarshalHexAux_CryptoKeyGen(keyGenString,
                                                          randomSource,
                                                          certainty);

        final String pGroupString = protocolInfo.getStringValue("pgroup");
        pGroup = Marshalizer.unmarshalHexAux_PGroup(pGroupString,
                                                    randomSource,
                                                    certainty);
    }
}
