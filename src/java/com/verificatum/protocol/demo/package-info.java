
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

/**
 *
 * This package gives a unified way to demonstrate and unit test
 * cryptographic protocols. The main things that distinguishes a
 * demonstration from a real execution are the following:
 *
 * <ol>
 *
 * <li> Each party is executed as a {@link java.lang.Runnable}. Thus,
 * all parties are simulated locally within a single JVM.
 *
 * <li> The configuration files used by a party are typically
 * generated automatically, based on the options provided to the
 * demonstrator.
 *
 * </ol>

 * To demonstrate an existing protocol, the programmer must implement
 * the interface {@link
 * com.verificatum.protocol.demo.DemoProtocolElGamalFactory}. This
 * interface allows {@link com.verificatum.protocol.demo.Demo} to set up
 * and execute a demonstration.
 */

package com.verificatum.protocol.demo;
