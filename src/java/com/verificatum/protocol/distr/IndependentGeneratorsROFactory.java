
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.distr;

import com.verificatum.protocol.elgamal.ProtocolElGamal;

/**
 * This is a factory class for {@link IndependentGeneratorsRO}, i.e.,
 * it can be instantiated to create instances of this class.
 *
 * @author Douglas Wikstrom
 */
public final class IndependentGeneratorsROFactory
    implements IndependentGeneratorsFactory {

    @Override
    public IndependentGenerators
        newInstance(final String sid, final ProtocolElGamal protocol) {
        return new IndependentGeneratorsRO(sid,
                                           protocol.getROHashfunction(),
                                           protocol.getGlobalPrefix(),
                                           protocol.getStatDist());
    }
}
