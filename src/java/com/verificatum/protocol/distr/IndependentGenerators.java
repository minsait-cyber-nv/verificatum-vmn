
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.distr;

import com.verificatum.arithm.PGroup;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.ui.Log;

/**
 * Represents a protocol which jointly generates a list of
 * "independent" generators.
 *
 * @author Douglas Wikstrom
 */
public interface IndependentGenerators {

    /**
     * Generate the independent generators.
     *
     * @param log Logging context.
     * @param pGroup Group to which the generator belongs.
     * @param numberOfGenerators Number of generators.
     * @return Independent generators.
     */
    PGroupElementArray generate(Log log, PGroup pGroup, int numberOfGenerators);
}
