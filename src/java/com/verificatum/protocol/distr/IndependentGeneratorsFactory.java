
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.distr;

import com.verificatum.protocol.elgamal.ProtocolElGamal;

/**
 * This is a joint factory class for {@link IndependentGeneratorsI}
 * and {@link IndependentGeneratorsRO}, i.e., it can be instantiated
 * to create instances of one of these classes.
 *
 * @author Douglas Wikstrom
 */
public interface IndependentGeneratorsFactory {

    /**
     * Creates an instance of the factory.
     *
     * @param sid Session identifier of the created instance.
     * @param protocol Protocol which invokes the created instance.
     * @return An instance of the protocol with the given parent
     * protocol and session identifier.
     */
    IndependentGenerators newInstance(String sid,
                                      ProtocolElGamal protocol);
}
