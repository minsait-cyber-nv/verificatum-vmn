
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.distr;

import java.util.Arrays;

import com.verificatum.arithm.PGroup;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.crypto.Hashdigest;
import com.verificatum.crypto.Hashfunction;
import com.verificatum.crypto.PRG;
import com.verificatum.crypto.PRGHeuristic;
import com.verificatum.crypto.RandomOracle;
import com.verificatum.eio.ByteTree;
import com.verificatum.eio.ExtIO;
import com.verificatum.ui.Log;


/**
 * Uses a "random oracle" to derive a list of "independent"
 * generators, i.e., a list of generators for which finding any
 * non-trivial representation implies that the discrete logarithm
 * assumption is violated.
 *
 * @author Douglas Wikstrom
 */
public final class IndependentGeneratorsRO implements IndependentGenerators {

    /**
     * Hashfunction on which the "random oracle" is based.
     */
    Hashfunction roHashfunction;

    /**
     * Prefix used with each invocation of the random oracle.
     */
    byte[] globalPrefix;

    /**
     * Decides the statistical distance from the uniform distribution
     * assuming that the random oracle is truly random.
     */
    int rbitlen;

    /**
     * Session identifier distinguishing this derivation from other.
     */
    String sid;

    /**
     * Creates an instance. It is the responsibility of the user to
     * ensure that the session identifier is unique among all
     * applications that should give different "independent" arrays of
     * generators.
     *
     * @param sid Session identifier which separates this derivation
     * from others.
     * @param roHashfunction Hashfunction on which the random oracle
     * is based.
     * @param globalPrefix Prefix used with each invocation of the
     * random oracle used to derive the independent
     * generators.
     * @param rbitlen Decides the statistical distance from the
     * uniform distribution assuming that the random oracle
     * is truly random.
     */
    public IndependentGeneratorsRO(final String sid,
                                   final Hashfunction roHashfunction,
                                   final byte[] globalPrefix,
                                   final int rbitlen) {
        this.sid = sid;
        this.roHashfunction = roHashfunction;
        this.globalPrefix = Arrays.copyOf(globalPrefix, globalPrefix.length);
        this.rbitlen = rbitlen;
    }

    /**
     * Generate the independent generators.
     *
     * @param log Logging context.
     * @param pGroup Underlying group.
     * @param numberOfGenerators Number of generators to generate.
     * @return Independent generators.
     */
    @Override
    public PGroupElementArray generate(final Log log,
                                       final PGroup pGroup,
                                       final int numberOfGenerators) {
        if (log != null) {
            log.info("Derive independent generators using RO.");
        }

        final PRG prg = new PRGHeuristic(roHashfunction);
        final RandomOracle ro = new RandomOracle(roHashfunction,
                                                 8 * prg.minNoSeedBytes());

        final Hashdigest d = ro.getDigest();
        d.update(globalPrefix);
        d.update(new ByteTree(ExtIO.getBytes(sid)).toByteArray());

        final byte[] seed = d.digest();

        prg.setSeed(seed);

        return pGroup.randomElementArray(numberOfGenerators, prg, rbitlen);
    }
}
