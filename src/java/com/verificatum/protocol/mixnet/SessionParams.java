
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.mixnet;

/**
 * Session parameters.
 *
 * @author Douglas Wikstrom
 */
public final class SessionParams {

    /**
     * Type of proof.
     */
    final String type;

    /**
     * Auxiliary session identifier.
     */
    final String auxsid;

    /**
     * Width of ciphertexts.
     */
    final int width;

    /**
     * Indicates that decryption is verified.
     */
    final boolean dec;

    /**
     * Indicates pre-computation for a shuffle is verified.
     */
    final boolean posc;

    /**
     * Indicates that a shuffle is verified.
     */
    final boolean ccpos;

    /**
     * Session parameters.
     *
     * @param type Type of proof.
     * @param auxsid Auxiliary session identifier.
     * @param width Width of ciphertexts.
     * @param dec Indicates that decryption is verified.
     * @param posc Indicates pre-computation for a shuffle is
     * verified.
     * @param ccpos Indicates that a shuffle is verified.
     */
    SessionParams(final String type,
                  final String auxsid,
                  final int width,
                  final boolean dec,
                  final boolean posc,
                  final boolean ccpos) {
        this.type = type;
        this.auxsid = auxsid;
        this.width = width;
        this.dec = dec;
        this.posc = posc;
        this.ccpos = ccpos;
    }
}
