
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.mixnet;

import java.io.File;

import com.verificatum.protocol.ProtocolBBGen;
import com.verificatum.protocol.ProtocolFormatException;
import com.verificatum.protocol.com.BullBoardBasicGen;
import com.verificatum.protocol.elgamal.ProtocolElGamalInterfaceFactory;
import com.verificatum.ui.info.InfoGenerator;


/**
 * Factory for interfaces of an El Gamal protocol.
 *
 * @author Douglas Wikstrom
 */
public final class MixNetElGamalInterfaceFactory
    extends ProtocolElGamalInterfaceFactory {

    /**
     * Return the info generator of this factory.
     *
     * @param protocolInfoFile Protocol info file.
     * @return Info generator.
     *
     * @throws ProtocolFormatException If the input is not the name of
     * a file from which a valid bulletin board can be derived.
     */
    @Override
    public InfoGenerator getGenerator(final File protocolInfoFile)
        throws ProtocolFormatException {

        final BullBoardBasicGen bullBoardBasicGen =
            ProtocolBBGen.getBullBoardBasicGen(protocolInfoFile);

        return new MixNetElGamalGen(bullBoardBasicGen);
    }
}
