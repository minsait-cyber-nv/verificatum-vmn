
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.elgamal;

/**
 * Container class storing the lower (inclusive) and upper (exclusive)
 * bound of the indices of a subarray of an array.
 *
 * @author Douglas Wikstrom
 */
class ProtocolElGamalRearInterval {

    /**
     * Starting position.
     */
    final int start;

    /**
     * Ending position.
     */
    final int end;

    /**
     * Container class storing the starting and ending index of a
     * subarray of an array.
     *
     * @param start Starting position.
     * @param end Ending position.
     */
    ProtocolElGamalRearInterval(final int start, final int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return String.format("%d-%d", start, end);
    }
}
