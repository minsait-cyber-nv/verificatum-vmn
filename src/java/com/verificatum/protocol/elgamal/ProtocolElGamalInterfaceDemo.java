
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.elgamal;

import java.io.File;

import com.verificatum.arithm.PGroupElement;
import com.verificatum.crypto.RandomSource;


/**
 * Interface of an El Gamal mix-net with the ability to generate demo
 * ciphertexts.
 *
 * @author Douglas Wikstrom
 */
public interface ProtocolElGamalInterfaceDemo {

    /**
     * Generates the given number of ciphertexts.
     *
     * @param fullPublicKey Full public key.
     * @param noCiphs Number of ciphertexts to generate.
     * @param outputFile Destination of generated ciphertexts.
     * @param randomSource Source of randomness.
     */
    void demoCiphertexts(PGroupElement fullPublicKey,
                         int noCiphs,
                         File outputFile,
                         RandomSource randomSource);
}
