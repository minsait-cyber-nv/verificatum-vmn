
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.elgamal;

/**
 * Container class identifying a component of a group element (or
 * array of group elements) within a list of group elements (or list
 * of arrays of group elements). This is used by {@link
 * ProtocolElGamalRearTool}. Two indexes are used: one for identifying
 * a group element (or group element array) within a list of group
 * elements (or group element arrays) and a second index to identify a
 * component thereof.
 *
 * @author Douglas Wikstrom
 */
class ProtocolElGamalRearPosition {

    /**
     * Index identifying a source group element (or array of group
     * elements).
     */
    final int source;

    /**
     * Index identifying a component within a source group element (or
     * array of group elements).
     */
    final int index;

    /**
     * Container class identifying a subarray within a list of source
     * arrays.
     *
     * @param source Index identifying a source group element (or
     * array of group elements).
     * @param index Index identifying a component of a group element
     * (or an array of group elements).
     */
    ProtocolElGamalRearPosition(final int source, final int index) {
        this.source = source;
        this.index = index;
    }

    @Override
    public String toString() {
        return String.format("(%d,%d)", source, index);
    }
}
