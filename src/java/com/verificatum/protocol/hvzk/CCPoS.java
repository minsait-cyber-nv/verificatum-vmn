
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import com.verificatum.arithm.PGroupElement;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.arithm.PRingElement;
import com.verificatum.arithm.PRingElementArray;
import com.verificatum.arithm.Permutation;
import com.verificatum.ui.Log;

/**
 * Interface for a commitment-consistent proof of a shuffle.
 *
 * @author Douglas Wikstrom
 */
public interface CCPoS {

    /**
     * Execute prover.
     *
     * @param log Logging context.
     * @param g Standard generator.
     * @param h Independent generators.
     * @param u Permutation commitment.
     * @param pkey Public key used to re-encrypt.
     * @param w List of ciphertexts.
     * @param wp List of ciphertexts.
     * @param r Commitment exponents.
     * @param pi Permutation.
     * @param s Random exponents used to process ciphertexts.
     */
    void prove(Log log,
               PGroupElement g,
               PGroupElementArray h,
               PGroupElementArray u,
               PGroupElement pkey,
               PGroupElementArray w,
               PGroupElementArray wp,
               PRingElementArray r,
               Permutation pi,
               PRingElementArray s);

    /**
     * Execute verifier.
     *
     * @param log Logging context.
     * @param l Index of prover.
     * @param g Standard generator.
     * @param h Independent generators.
     * @param u Permutation commitment.
     * @param pkey Public key used to re-encrypt.
     * @param w List of ciphertexts.
     * @param wp List of ciphertexts.
     * @param raisedu Permutation commitment raised to a secret
     * exponent.
     * @param raisedh Independent generators raised to a secret
     * exponent.
     * @param raisedExponent Secret exponent.
     * @return Verdict about the proof.
     */
    boolean verify(Log log,
                   int l,
                   PGroupElement g,
                   PGroupElementArray h,
                   PGroupElementArray u,
                   PGroupElement pkey,
                   PGroupElementArray w,
                   PGroupElementArray wp,
                   PGroupElementArray raisedu,
                   PGroupElementArray raisedh,
                   PRingElement raisedExponent);
}
