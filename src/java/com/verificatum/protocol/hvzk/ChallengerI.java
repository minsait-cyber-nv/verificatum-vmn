
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.protocol.coinflip.CoinFlipPRingSource;
import com.verificatum.ui.Log;

/**
 * Container class for a coin-flipping functionality used to generate
 * challenges for public coin protocols.
 *
 * @author Douglas Wikstrom
 */
public final class ChallengerI implements Challenger {

    /**
     * Source of jointly generated random coins.
     */
    CoinFlipPRingSource coins;

    /**
     * Creates an instance which generates challenges using the given
     * source of jointly generated random coins.
     *
     * @param coins Source of jointly generated random coins.
     */
    public ChallengerI(final CoinFlipPRingSource coins) {
        this.coins = coins;
    }

    // Documented in Challenger.java

    @Override
    public byte[] challenge(final Log log, final ByteTreeBasic data,
                            final int vbitlen, final int rbitlen) {

        log.info("Generate bits jointly.");
        final Log tempLog = log.newChildLog();

        return coins.getCoinBytes(tempLog, vbitlen, rbitlen);
    }
}
