
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import com.verificatum.arithm.PGroupElement;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.arithm.PRingElementArray;
import com.verificatum.arithm.Permutation;
import com.verificatum.ui.Log;

/**
 * Interface for a round of proofs of shuffles.
 *
 * @author Douglas Wikstrom
 */
public interface PoSCMulti {

    /**
     * Execute proofs.
     *
     * @param log Logging context.
     * @param g Standard generator.
     * @param generators Independent generators.
     * @param permutationCommitments Permutation commitment.
     * @return Array of verdicts.
     */
    boolean[] execute(Log log,
                      PGroupElement g,
                      PGroupElementArray generators,
                      PGroupElementArray[] permutationCommitments);

    /**
     * Execute proofs.
     *
     * @param log Logging context.
     * @param g Standard generator.
     * @param generators Independent generators.
     * @param permutationCommitments Permutation commitment.
     * @param commitmentExponents Commitment exponents.
     * @param permutation Permutation.
     * @return Array of verdicts.
     */
    boolean[] execute(Log log,
                      PGroupElement g,
                      PGroupElementArray generators,
                      PGroupElementArray[] permutationCommitments,
                      PRingElementArray commitmentExponents,
                      Permutation permutation);
}
