
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import java.io.File;

import com.verificatum.protocol.elgamal.ProtocolElGamal;


/**
 * Factory for {@link CCPoSW}.
 *
 * @author Douglas Wikstrom
 */
public final class CCPoSWFactory implements CCPoSFactory {

    /**
     * Returns a new instance with the given session identifier and
     * parent protocol.
     *
     * @param sid Session identifier.
     * @param protocol Parent protocol.
     * @param rosid Session identifier for random oracle proofs.
     * @param nizkp Destination directory for random oracle
     * proofs. Note that this directory is deleted when {@link
     * com.verificatum.protocol.Protocol#deleteState()} is called.
     * @return Instance of a proof of a shuffle.
     */
    @Override
    public CCPoS newPoS(final String sid,
                        final ProtocolElGamal protocol,
                        final String rosid,
                        final File nizkp) {
        return new CCPoSW(sid, protocol, rosid, nizkp);
    }
}
