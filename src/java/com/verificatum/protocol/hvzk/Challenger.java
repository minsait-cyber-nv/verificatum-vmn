
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import com.verificatum.eio.ByteTreeBasic;
import com.verificatum.ui.Log;

/**
 * Interface capturing the challenger of a public-coin protocol.
 *
 * @author Douglas Wikstrom
 */
public interface Challenger {

    /**
     * Returns a challenge.
     *
     * @param log Logging context.
     * @param data Input to the random oracle, if this instance
     * generates its challenges using one. This should
     * contain the instance and the messages up to the
     * challenge step.
     * @param vbitlen Number of bits to generate.
     * @param rbitlen Decides the statistical distance from the
     * uniform distribution.
     * @return Challenge bytes.
     */
    byte[] challenge(Log log,
                     ByteTreeBasic data,
                     int vbitlen,
                     int rbitlen);
}
