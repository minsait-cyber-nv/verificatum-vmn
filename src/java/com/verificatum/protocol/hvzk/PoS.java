
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

package com.verificatum.protocol.hvzk;

import com.verificatum.arithm.PGroupElement;
import com.verificatum.arithm.PGroupElementArray;
import com.verificatum.arithm.PRingElementArray;
import com.verificatum.arithm.Permutation;
import com.verificatum.ui.Log;

/**
 * Interface for a proof of a shuffle.
 *
 * @author Douglas Wikstrom
 */
public interface PoS {

    /**
     * Performs precomputation of prover.
     *
     * @param log Logging context.
     * @param g Standard generator.
     * @param h Independent generators.
     * @param pi Permutation.
     */
    void precompute(Log log,
                    PGroupElement g,
                    PGroupElementArray h,
                    Permutation pi);

    /**
     * Execute prover.
     *
     * @param log Logging context.
     * @param pkey Public key used to construct the homomorphism.
     * @param w List of ciphertexts.
     * @param wp List of ciphertexts.
     * @param s Random exponents used to process ciphertexts.
     */
    void prove(Log log,
               PGroupElement pkey,
               PGroupElementArray w,
               PGroupElementArray wp,
               PRingElementArray s);

    /**
     * Performs precomputation of the verifier.
     *
     * @param log Logging context.
     * @param g Standard generator.
     * @param h Independent generators.
     */
    void precompute(Log log, PGroupElement g, PGroupElementArray h);

    /**
     * Execute verifier.
     *
     * @param log Logging context.
     * @param l Index of prover.
     * @param pkey Public key used to construct the homomorphism.
     * @param w List of ciphertexts.
     * @param wp List of ciphertexts.
     * @return Verdict about the proof.
     */
    boolean verify(Log log,
                   int l,
                   PGroupElement pkey,
                   PGroupElementArray w,
                   PGroupElementArray wp);

    /**
     * Releases resources allocated by this instance.
     */
    void free();
}
