
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
 * software. It is distributed under Verificatum License 1.0 and
 * Verificatum License Appendix 1.0 for VMN.
 *
 * You should have agreed to this license and appendix when
 * downloading VMN and received a copy of the license and appendix
 * along with VMN. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VMN in any way and you must delete
 * VMN immediately.
 */

import java.lang.reflect.*;

/**
 * Implements a class that tries to load a Java class of a given
 * version. This is used by the configure script.
 *
 * @author Douglas Wikstrom
 */
public class TestLoadJar {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        String className = args[0];
	Class klass = null;

	try {
	    klass = Class.forName(className);
	} catch (ClassNotFoundException cnfe) {
	    System.out.println("Cannot locate the class " + className + "!");
	} catch (SecurityException se) {
	    System.out.println("Not allowed to load the native library needed "
                               + "by " + className + " to run in native mode!");
	} catch (UnsatisfiedLinkError ule) {
	    System.out.println("Missing native library needed by "
                               + className + "!");
	} catch (IllegalArgumentException iare) {
	    System.out.println("This is a bug in the building system!");
	}

        if (klass != null) {
            final String ev = args[1];
            final String av = klass.getPackage().getSpecificationVersion();

            if (!av.equals(ev)) {
                System.out.println("Wrong version number ("
                                   + av + "), requires " + ev + "!");
            }
        }
    }
}
