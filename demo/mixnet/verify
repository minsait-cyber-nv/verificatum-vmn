#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Mix-Net (VMN). VMN is NOT free
# software. It is distributed under Verificatum License 1.0 and
# Verificatum License Appendix 1.0 for VMN.
#
# You should have agreed to this license and appendix when
# downloading VMN and received a copy of the license and appendix
# along with VMN. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMN
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VMN in any way and you must delete
# VMN immediately.

###########################################################################
################### Execute standalone verifier. ##########################
###########################################################################

. ./checkvmn

. ./conf

if test x$AUXSID = x;
then
    AUXSIDDIR=default
else
    AUXSIDDIR=$AUXSID
fi

if test x$SILENT = x;
then
    VERBOSE=-v
else
    VERBOSE=
fi

# Make sure we log everything.
{

if test $CORRECTNESS = "noninteractive"
then

    phasestart "Copying proof from Party01 (verifier is not executing yet)"

    printf "
rm -rf export
mkdir export
cd export
cp -R ../dir/nizkp/$AUXSIDDIR .
cp ../protInfo.xml .
cd ..
" > $TMP_COMMAND

    cpevalat $TMP_COMMAND 1

    # This convoluted way of invoking copyfrom is needed due to a bug
    # in /bin/sh.
    rm -rf export
    bash -c "source conf; copyfrom 1 export ."

    phaseend

    phasestart "Verify Fiat-Shamir Proof"

    # Make sure we log everything.
    {

	printf "\n"

	vmnv -sloppy $VERBOSE -v -e -wd "/tmp/verificatum/$$" -a $ARRAYS export/protInfo.xml export/$AUXSIDDIR

	EXITCODE=$?
	rm -rf "/tmp/verificatum/$$"

	test $EXITCODE = 0 || exit $EXITCODE

    # Make sure we log everything.
    } >> vmnv_log

    phaseend

else

    printf "\nERROR!"
    printf "You can not verify an execution that used *interactive* "
    printf "zero-knowledge proofs. Please edit the CORRECTNESS variable"
    printf "in the conf-file to try out *non-interactive* proofs in the"
    printf "random oracle model.\n\n"

fi

# Make sure we log everything.
} | tee -a demo_log

